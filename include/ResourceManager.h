/*
 * ResourceManager.h
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#pragma once

#include <map>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_image/SDL_image.h>
#include <memory>

#include "Texture.h"
#include "Utility.h"

//TODO convert to single class for all with template classes
class ResourceManager {
private:
	std::map<std::string, Texture*> m_textures;
	std::map<std::string, TTF_Font*> m_font;
	SDL_Renderer* m_renderer;

public:
	int count() { return m_textures.size(); }

	ResourceManager();
	~ResourceManager();

	void setRenderer(SDL_Renderer* renderer) { m_renderer = renderer; }

	// Add functions
	bool addTexture(const std::string texture);
	bool addTexture(const std::string resource, Texture* texture);
	bool addFont(const std::string resource);

	// Remove
	bool removeTexture(const std::string texture);

	// Get functions
	Texture* getTexture(const std::string texture);
	TTF_Font* getFont(const std::string resource);

	// Cleanup function
	void cleanup();
};


