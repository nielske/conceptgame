/*
 * Component.h
 *
 *  Created on: Jun 22, 2018
 *      Author: niels
 */

#ifndef ENTITYSYSTEM_COMPONENT_H_
#define ENTITYSYSTEM_COMPONENT_H_

class Component {
protected:
	Component() {

	}
	virtual ~Component() {

	}
public:
	virtual void update() = 0;
	virtual void render(int x, int y) = 0;
};

#endif /* ENTITYSYSTEM_COMPONENT_H_ */
