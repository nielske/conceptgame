/*
 * Game.h
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include <SDL2_image/SDL_image.h>
#include <SDL2/SDL.h>
#include <json.hpp>
#include <Utility.h>
#include <ResourceManager.h>
#include <World.h>
#include <Settings.h>
#include <imgui/imgui.h>

class State;
using json = nlohmann::json;

enum class GameStatus {
	RUNNING, STARTING, STOPPING, ERROR, PAUSED
};

class Game {
private:
	SDL_Window* m_window = NULL;
	SDL_Renderer* m_renderer = NULL;
	std::vector<State*> m_states;
	ResourceManager m_resources;
	Settings m_settings;
	World m_world;

	SDL_Event m_event;
	GameStatus m_status;

	int m_width = 1280;
	int m_height = 720;

	void handleEvents();
	void update();
	void render();

public:
	Game();
	~Game();

	GameStatus init();
	GameStatus run();

	void changeState( State* state );
	void pushState( State* state );
	void popState();

	World* getWorld() {
		return &m_world;
	}

	ResourceManager* getRM() {
		return &m_resources;
	}

	SDL_Renderer* getRenderer() {
		return m_renderer;
	}

	Settings* getSettings() {
		return &m_settings;
	}

	std::pair<int, int> getWindowSize() {
		return std::make_pair(m_width, m_height);
	}

	void setStatus(GameStatus status) {
		m_status = status;
	}
};
