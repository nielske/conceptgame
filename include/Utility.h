/*
 * Utility.h
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#pragma once

#include <string>
#include <fstream>
#include <ctime>
#include <SDL2/SDL.h>
#include <cstdio>
#include <iostream>

#ifdef __unix__
#include <sys/utsname.h>
#endif

//TODO: better printing to file
#define INFO(msg) \
        std::cout << "\033[0;36mINFO: " << msg << "\033[0m" << std::endl; \
        Utility::logMessage(msg, false);

#define DEBUG(msg) \
        std::cout << "\033[0;33mDEBUG: " << __FILE__ << ":" << __LINE__ << ": " << __func__ << ": "; \
        std::cout << msg << "\033[0m" << std::endl; \
        Utility::logMessage(msg, false);

#define DEBUG_POSITION(x, y) \
        std::cout << "\033[0;33mDEBUG: " << __FILE__ << ":" << __LINE__ << ": " << __func__ << ": "; \
        std::cout << "X: " << x << ", Y:" << y << "\033[0m" << std::endl;

#define ERROR(msg) \
        std::cout << "\033[0;31mERROR: " << __FILE__ << ":" << __LINE__ << ": " << __func__ << ": "; \
        std::cout << msg << "\033[0m" << std::endl; \
        Utility::logMessage(msg, true);

typedef std::pair<int, int> COORDINATE;

namespace Utility {
    extern std::string BIN_PATH;
    extern std::string ROOT_PATH;
    extern std::string DATA_PATH;
    extern std::string EXE_PATH;
    const extern std::string APPLICATION_VERSION;
    const extern std::string OPERATING_SYSTEM;

    // Get accurate linux operating system version.
    inline std::string getLinuxOperatingSystem() {
        // /proc/version contains the complete OS version on most linux systems.
        const std::string osfile = "/proc/version";
        std::ifstream in(osfile.c_str());
        
        if (!in) {
            // Cannot open file or file is corrupted. Default to generic Linux.
            return "Linux";
        } else {
            std::string str((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
            return str;
        }
    }
    
    // Get accurate date and time
    inline std::string getDateTime() {
        std::time_t now = std::time(0);
        std::tm *localtime = std::localtime(&now);
        std::string dt = "";

        if (localtime->tm_mday < 10) {
            dt += "0" + std::to_string(localtime->tm_mday);
        } else {
            dt += std::to_string(localtime->tm_mday);
        }

        if (localtime->tm_mon < 10) {
            dt += "-0" + std::to_string(1 + localtime->tm_mon);
        } else {
            dt += "-" + std::to_string(1 + localtime->tm_mon);
        }

        dt += "-" + std::to_string(1900 + localtime->tm_year);

        if (localtime->tm_hour < 10) {
            dt += " 0" + std::to_string(localtime->tm_hour);
        } else {
            dt += " " + std::to_string(localtime->tm_hour);
        }

        if (localtime->tm_min < 10) {
            dt += ":0" + std::to_string(localtime->tm_min);
        } else {
            dt += ":" + std::to_string(localtime->tm_min);
        }

        if (localtime->tm_sec < 10) {
            dt += ":0" + std::to_string(localtime->tm_sec);
        } else {
            dt += ":" + std::to_string(localtime->tm_sec);
        }

        return dt;
    }

    template <typename T>
    inline void logMessage(const T &msg, bool error = false, bool append = true) {
        // Determine log path
        const std::string &path = ROOT_PATH + "/Logfile.txt";
        std::ofstream out;

        // Check if new log must be generated
        if (append) {
            out.open(path.c_str(), std::ios_base::out | std::ios_base::app);
        } else {
            out.open(path.c_str(), std::ios_base::out | std::ios_base::trunc);
        }

        // Write ticks
        int ticks = SDL_GetTicks();
        int remainder = ticks % 1000;
        int seconds = (ticks - remainder) / 1000;

        out << "[" << seconds << "." << remainder << "] ";

        // Write message
        out << msg;

        if (error) {
            out << ". Please report this error to the developer.";
        }

        // End stream
        out << "\n";
        out.close();
    }

    inline void logVitals() {
        // VITALS
        // Time / date / application version
        // Operating System
        // Pathing information
        // System info: cpu, ram, gpu
        // Display options

        // Application information
        INFO(getDateTime() + "; ConceptGame " + APPLICATION_VERSION);
        logMessage(getDateTime() + "; ConceptGame " + APPLICATION_VERSION, false, false);

        // Operating System
        INFO("Operating system: " + OPERATING_SYSTEM);
        
        // CPU
#ifdef __unix__
        struct utsname name;
        if(!uname(&name)) {
            std::string cpu = name.machine;
            INFO("CPU Architecture: " + cpu)
        } else {
            INFO("CPU Architecture: Unknown")
        }
#endif
        INFO("CPU Logical Cores: " + SDL_GetCPUCount())
        
        
        // GPU
        // MEM

        // Pathing information
        INFO("Write path: " + ROOT_PATH);
        INFO("Read path: " + DATA_PATH);
    }

    inline void fill(SDL_Renderer* renderer, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
    }
}
