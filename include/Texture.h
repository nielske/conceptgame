/*
 * Texture.h
 *
 *  Created on: Mar 17, 2018
 *      Author: niels
 */

#pragma once

#include <SDL2/SDL.h>
#include <string>

class Texture {
private:
	SDL_Renderer* m_renderer = nullptr;
	SDL_Texture* m_texture = nullptr;
	int m_width = 0;
	int m_height = 0;

public:
	Texture( SDL_Renderer* renderer, int width = 0, int height = 0, SDL_Texture* texture = nullptr);
	Texture() {
	}
	~Texture();

	int getWidth();
	int getHeight();

	SDL_Texture* getTexture() {
		return m_texture;
	}

	bool loadFromFile( std::string path );
	bool loadFromRenderedText( std::string renderedText, SDL_Color textColor );

	void freeMemory();
	void setAlpha( Uint8 alpha );
	void setRenderer( SDL_Renderer* renderer );
	void render( int x, int y, SDL_Rect* clip = nullptr, double angle = 0.0, SDL_Point* center = nullptr, SDL_RendererFlip flip =
			SDL_FLIP_NONE );
};
