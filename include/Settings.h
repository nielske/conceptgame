/*
 * Settings.h
 *
 *  Created on: Jun 1, 2018
 *      Author: niels
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <string>
#include <map>
#include <vector>
#include <sstream>

class Settings {
private:
	// Return string in type of T
	template<typename T>
	T convertToType( const std::string &input ) const;

	// Return string of type T
	template<typename T>
	std::string convertToStr( const T input ) const;

	bool read();
	bool write() const;

	std::pair<std::string, std::string> parseLine( const std::string &line ) const;

	bool m_isChanged;
	std::string m_file;
	std::map<std::string, std::string> m_data;
	const std::locale m_locale;

public:
	Settings();
	~Settings();

	bool load( const std::string &file );
	bool save();
	bool isChanged() const;

	template<typename T>
	void get( const std::string &key, T &value ) const;

	template<typename T>
	void get( const std::string &key, std::vector<T> &value ) const;

	template<typename T>
	void set( const std::string &key, const T value );

	template<typename T>
	void set( const std::string &key, const std::vector<T> value );

	void print() const;
};

/////
///// Settings::convertToStr
/////

template<typename T>
inline std::string Settings::convertToStr( const T input ) const {
	throw "Unsupported type supplied, either change types, or write a correct conversion function for the template type.";
}

template<>
inline std::string Settings::convertToStr<std::string>( std::string value ) const {
	return value;
}

template<>
inline std::string Settings::convertToStr<const char*>( const char* value ) const {
	return std::string(value);
}

template<>
inline std::string Settings::convertToStr<bool>( bool value ) const {
	return (value) ? "true" : "false";
}

template<>
inline std::string Settings::convertToStr<char>( char value ) const {
	std::string tmp = "";
	tmp = value;
	return tmp;
}

template<>
inline std::string Settings::convertToStr<int>( int value ) const {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

template<>
inline std::string Settings::convertToStr<float>( float value ) const {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

template<>
inline std::string Settings::convertToStr<short>( short value ) const {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

template<>
inline std::string Settings::convertToStr<double>( double value ) const {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

/////
///// Settings::convertToType
/////

template<typename T>
inline T Settings::convertToType( const std::string& input ) const {
	throw "Unconvertable type encountered, please use a different type, or define the handle case in Settings.h";
}

template<>
inline int Settings::convertToType<int>( const std::string &input ) const {
	int value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline double Settings::convertToType<double>( const std::string &input ) const {
	double value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline float Settings::convertToType<float>( const std::string &input ) const {
	float value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline short Settings::convertToType<short>( const std::string &input ) const {
	short value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline bool Settings::convertToType<bool>( const std::string &input ) const {
	return input == "true" ? true : false;
}

template<>
inline char Settings::convertToType<char>( const std::string &input ) const {
	return input[0];
}

template<>
inline std::string Settings::convertToType<std::string>( const std::string &input ) const {
	return input;
}

template<typename T>
inline void Settings::get( const std::string& key, T& value ) const {
	auto it = m_data.find(key);

	if ( it != m_data.end() ) {
		value = convertToType<T>(it->second);
	}
}

template<typename T>
inline void Settings::get( const std::string& key, std::vector<T>& value ) const {
	auto it = m_data.find(key);

	if ( it != m_data.end() ) {
		std::string output;
		std::istringstream parser(it->second);

		value.clear();

		//split by comma
		while ( getline(parser, output, ',') ) {
			value.push_back(convertToType<T>(output));
		}
	}
}

template<typename T>
inline void Settings::set( const std::string& key, const T value ) {
	m_data[key] = convertToStr<T>(value);
	m_isChanged = true;
}

template<typename T>
inline void Settings::set( const std::string& key, const std::vector<T> value ) {
	// transform the vector into a string that seperates the elements with a comma
	std::string valueAsString;

	for ( size_t i = 0; i < value.size() - 1; ++i ) {
		valueAsString += convertToStr<T>(value.at(i)) + ",";
	}

	valueAsString += convertToStr<T>(value.back());

	// the [] operator replaces the value if the key is found, if not it creates a new element
	m_data[key] = valueAsString;
	m_isChanged = true;
}

#endif /* SETTINGS_H_ */
