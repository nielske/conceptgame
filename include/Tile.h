/*
 * Tile.h
 *
 *  Created on: May 21, 2018
 *      Author: niels
 */

#ifndef TILE_H_
#define TILE_H_

#include "Texture.h"

class Tile {
private:
	Texture* m_texture;
	SDL_Color m_color;

public:
	Tile();
	Tile( Texture* texture, SDL_Color color );
	~Tile();

	void update();
	void render( int x, int y );

	SDL_Color getColor() {
		return m_color;
	}
};

#endif /* TILE_H_ */
