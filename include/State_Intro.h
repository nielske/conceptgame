/*
 * Intro.h
 *
 *  Created on: Mar 16, 2018
 *      Author: niels
 */

#include <map>
#include "State.h"

#pragma once

class Intro: public State {
private:
	static Intro m_Intro;
	World* m_world;

	std::pair<int, int> m_coords = std::make_pair(250, 250);

protected:
	Intro() {
	}

public:
	void enter( Game* game );
	void exit( Game* game );

	void handleInput( Game* game );
	void update( Game* game );
	void render( Game* game );

	static Intro* Instance() {
		return &m_Intro;
	}
};
