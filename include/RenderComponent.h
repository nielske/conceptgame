/*
 * RenderComponent.h
 *
 *  Created on: Jun 22, 2018
 *      Author: niels
 */

#ifndef ENTITYSYSTEM_RENDERCOMPONENT_H_
#define ENTITYSYSTEM_RENDERCOMPONENT_H_

#include <Texture.h>
#include <Component.h>
#include <Utility.h>

class RenderComponent : public Component {
private:
	Texture* m_texture;
	SDL_Color m_color;

public:
	RenderComponent(Texture* texture);
	~RenderComponent();

	void update();
	void render(int x, int y);
};

#endif /* ENTITYSYSTEM_RENDERCOMPONENT_H_ */
