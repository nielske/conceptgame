/*
 * Entity.h
 *
 *  Created on: Jun 22, 2018
 *      Author: niels
 */

#ifndef ENTITYSYSTEM_ENTITY_H_
#define ENTITYSYSTEM_ENTITY_H_

#include <vector>
#include <memory>
#include "Component.h"
#include "Utility.h"

class Entity {
private:
	std::vector<Component*> m_components;

public:
	Entity();
	~Entity();

	// Remove component by type // TODO
	void addComponent(Component* component);

	void update();
	void render(int x, int y);
};

#endif /* ENTITYSYSTEM_ENTITY_H_ */
