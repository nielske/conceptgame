/*
 * World.h
 *
 *  Created on: May 21, 2018
 *      Author: niels
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <noise/noise.h>
#include <map>
#include <Tile.h>
#include <Entity.h>
#include <RenderComponent.h>
#include <ResourceManager.h>
#include <Utility.h>
#include <Settings.h>

class World {
public:
	World( int width = 500, int height = 500 );
	~World();

	const int m_tileSize = 64;
	const int m_chunkSize = 8;

	bool m_updateMinimap = true;

	int getWidth() const {
		return m_width;
	}
	int getHeight() const {
		return m_height;
	}
	COORDINATE getPosition() const {
		return m_position;
	}

	void update();
	void render();
	void generate();
	void cleanup();

	void setRM( ResourceManager* RM ) {
		m_resources = RM;
	}

	void setRenderer( SDL_Renderer* renderer ) {
		m_renderer = renderer;
	}

	void setSettings( Settings* settings ) {
		m_settings = settings;
	}

	void setPosition( COORDINATE position ) {
		m_position = position;
	}

	Tile* getTile( int x, int y ) {
		auto search = m_world.find(COORDINATE(x, y));
		if (search != m_world.end()) {
			return m_world.at(COORDINATE(x, y));
		} else {
			return nullptr;
		}
	}

	Entity* getEntity(int x, int y) {
		auto search = m_entities.find(COORDINATE(x, y));
		if (search != m_entities.end()) {
			return m_entities.at(COORDINATE(x, y));
		} else {
			return nullptr;
		}
	}

private:
	SDL_Renderer* m_renderer;
	ResourceManager* m_resources;
	Settings* m_settings;
	int m_width;
	int m_height;
	noise::module::Perlin m_perlin;

	COORDINATE m_position;
	COORDINATE m_display;
	std::map<COORDINATE, Tile*> m_world;
	std::map<COORDINATE, Entity*> m_entities;
	Entity* m_player;

	double noise( double nx, double ny );
};

#endif /* WORLD_H_ */
