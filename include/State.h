/*
 * State.h
 *
 *  Created on: Mar 15, 2018
 *      Author: niels
 */

#pragma once

#include "Game.h"

class State {
protected:
	State() {
	}
	virtual ~State() {
	}

public:
	virtual void enter( Game* game ) = 0;
	virtual void exit( Game* game ) = 0;

	virtual void handleInput( Game* game ) = 0;
	virtual void update( Game* game ) = 0;
	virtual void render( Game* game ) = 0;

	void changeState( Game* game, State* state ) {
		game->changeState(state);
	}
};
