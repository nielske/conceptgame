/*
 * Tile.cpp
 *
 *  Created on: May 21, 2018
 *      Author: niels
 */

#include "Tile.h"

Tile::Tile() {
	m_texture = new Texture;
}

Tile::Tile( Texture* texture, SDL_Color color ) {
	m_texture = texture;
	m_color = color;
}

Tile::~Tile() {
	m_texture->freeMemory();
	m_texture = nullptr;
}

void Tile::render( int x, int y ) {
	m_texture->render(x, y);
}

void Tile::update() {
	if ( !m_texture ) {

	} else {

	}
}
