/*
 * Entity.cpp
 *
 *  Created on: Jun 22, 2018
 *      Author: niels
 */

#include "Entity.h"

Entity::Entity() {
	
}

Entity::~Entity() {

}

void Entity::addComponent(Component* component) {
	m_components.push_back(component);
}

void Entity::update() {
	for ( auto &c : m_components ) {
		c->update();
	}
}

void Entity::render(int x, int y) {
	for ( auto &c : m_components ) {
		c->render(x, y);
	}
}
