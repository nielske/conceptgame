/*
 * World.cpp
 *
 *  Created on: May 21, 2018
 *      Author: niels
 */

#include "World.h"

World::World( int width, int height ) :
		m_width(width), m_height(height) {
}

World::~World() {
	cleanup();
}

void World::generate() {
	if ( !m_width || !m_height ) {
		m_width = 500;
		m_height = 500;
	}

	// Clear current world
	for (auto it = m_world.begin(); it != m_world.end(); it++) {
		delete it->second;
		m_world.erase(it);
	}
	m_world.clear();

	std::srand(time(0));
	m_perlin.SetSeed(std::rand());

	for ( int x = 0; x < m_width; x++ ) {
		for ( int y = 0; y < m_height; y++ ) {
			double nx = x / (double) m_width - 0.5;
			double ny = y / (double) m_height - 0.5;

			double elevation = noise(nx, ny);

			Tile* tile;

			if ( elevation < 0.4 ) {
				SDL_Color color = { 0x00, 0x00, 0xFF, 0xFF };
				tile = new Tile(m_resources->getTexture("water.png"), color);
			} else if ( elevation < 0.6 ) {
				SDL_Color color = { 0xFF, 0xE1, 0x00, 0xFF };
				tile = new Tile(m_resources->getTexture("sand.png"), color);
			} else {
				SDL_Color color = { 0x00, 0xFF, 0x00, 0xFF };
				tile = new Tile(m_resources->getTexture("grass.png"), color);
			}

			m_world.insert(std::make_pair(COORDINATE(x, y), tile));
		}
	}

	m_position = COORDINATE(5, 5);

	// m_player = new Entity();
	// RenderComponent* rc = new RenderComponent(m_resources->getTexture("placeholder_inbound_mag.png"));
	// m_player->addComponent(rc);
	// m_entities.insert(std::make_pair(COORDINATE(5, 5), m_player));

	// Generate minimap
	SDL_Texture* mapTexture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, m_width, m_height);
	SDL_SetRenderTarget(m_renderer, mapTexture);
	SDL_RenderClear(m_renderer);

	for ( int x = 0; x < m_width; x++ ) {
		for ( int y = 0; y < m_height; y++ ) {
			SDL_Color color = getTile(x, y)->getColor();
			SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a);
			//TODO: scale texture
			SDL_RenderDrawPoint(m_renderer, x, y);
		}
	}

	// Blank minimap with only terrain
	Texture* tex = new Texture(m_renderer, m_width, m_height, mapTexture);
	m_resources->addTexture("minimap_blank", tex);

	// Create seperate texture
	SDL_Texture* mapTextureFilled = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, m_width, m_height);
	SDL_SetTextureBlendMode(mapTextureFilled, SDL_BLENDMODE_BLEND);
	SDL_SetRenderTarget(m_renderer, mapTextureFilled);

	// Add the rest to this one
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
	SDL_Rect player_rect = {m_position.first, m_position.second, 10, 10};
	SDL_RenderFillRect(m_renderer, &player_rect);

	// Finish it off
	SDL_SetRenderTarget(m_renderer, NULL);
	Texture* texFilled = new Texture(m_renderer, m_width, m_height, mapTextureFilled);
	m_resources->addTexture("minimap", texFilled);

	// Set display size
	m_settings->get("r_width", m_display.first);
	m_settings->get("r_height", m_display.second);
}

void World::cleanup() {
	for (auto it = m_world.begin(); it != m_world.end(); it++) {
		delete it->second;
		m_world.erase(it);
	}

	for (auto it = m_entities.begin(); it != m_entities.end(); it++) {
		delete it->second;
		m_entities.erase(it);
	}

	m_settings = nullptr;
	m_renderer = nullptr;
	m_resources = nullptr;
}

void World::update() {
	// Update minimap
	if (m_updateMinimap) {
		// Create seperate texture
		SDL_Texture* mapTexture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, m_width, m_height);
		SDL_SetTextureBlendMode(mapTexture, SDL_BLENDMODE_BLEND);
		SDL_SetRenderTarget(m_renderer, mapTexture);

		// Add the rest to this one
		SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
		SDL_Rect player_rect = {m_position.first, m_position.second, 10, 10};
		SDL_RenderFillRect(m_renderer, &player_rect);

		// Finish it off
		SDL_SetRenderTarget(m_renderer, NULL);
		Texture* tex = new Texture(m_renderer, m_width, m_height, mapTexture);
		m_resources->removeTexture("minimap");
		m_resources->addTexture("minimap", tex);
		m_updateMinimap = false;
	}
}

void World::render() {
	// How many tiles fit in the screen (round up)
	// Move this to load/generate/change resolution?
	int horizontalTiles = ceil(m_display.first / m_tileSize);
	int verticalTiles = ceil(m_display.second / m_tileSize);

	DEBUG_POSITION(m_position.first, m_position.second)

	// Draw tiles based on position
	for ( int x = m_position.first - (horizontalTiles / 2), dx = 0; x < m_position.first + horizontalTiles; x++, dx++ ) {
		for ( int y = m_position.second - (verticalTiles / 2), dy = 0; y < m_position.second + verticalTiles; y++, dy++ ) {
			if (getTile(x, y)) {
				getTile(x, y)->render(dx * m_tileSize - 32, dy * m_tileSize - 32);
			}
		}
	}	

	// Draw entities based on position
	for ( int x = 0; x < m_display.first / m_tileSize; x++) {
		for ( int y = 0; y < m_display.second / m_tileSize; y++) {
			if (getEntity(x, y)) {
				getEntity(x, y)->render(x * m_tileSize, y * m_tileSize);
			}
		}
	}

	SDL_Rect positionRect = {horizontalTiles / 2 * m_tileSize, verticalTiles / 2 * m_tileSize, 64, 64};
	SDL_SetRenderDrawColor(m_renderer, 0xFF, 0x00, 0x00, 0xFF);
	SDL_RenderDrawRect(m_renderer, &positionRect);
}

double World::noise( double nx, double ny ) {
	return m_perlin.GetValue(nx, ny, 0) / 2.0 + 0.5;
}
