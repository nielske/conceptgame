/*
 * Game.cpp
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#include "Game.h"
#include "State.h"
#include "State_Intro.h"

Game::Game() :
		m_status(GameStatus::STARTING) {

}

Game::~Game() {
	TTF_Quit();
	SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

GameStatus Game::init() {
	// Log vitals of the computer
	Utility::logVitals();

	// Initialize SDL
	if ( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		ERROR("SDL_Init failed to initialize")
		ERROR(SDL_GetError())
		return GameStatus::ERROR;
	}
	DEBUG("SDL_Init successful")

	// Load configuration
	if ( !m_settings.load(Utility::DATA_PATH + "/config.ini") ) {
		ERROR("Configuration file could not be loaded.")
	}

	m_settings.get("r_width", m_width);
	m_settings.get("r_height", m_height);
	bool fullscreen;
	m_settings.get("r_fullscreen", fullscreen);

	if ( fullscreen ) {
		SDL_DisplayMode dm;
		for ( int i = 0; i < SDL_GetNumVideoDisplays(); i++ ) {
			int shouldBeZero = SDL_GetCurrentDisplayMode(i, &dm);

			if ( shouldBeZero != 0 ) {
				// Error
				ERROR("Could not get display mode.")
				ERROR(SDL_GetError())
			} else {
				if ( dm.h > m_height ) {
					m_height = dm.h;
					m_settings.set("r_height", m_height);
				}

				if ( dm.w > m_width ) {
					m_width = dm.w;
					m_settings.set("r_width", m_width);
				}
				break;
			}
		}
	}

	// Create SDL Window
	m_window = SDL_CreateWindow("Concept Game", 0, 0, m_width, m_height, fullscreen ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_SHOWN);

	if ( m_window == nullptr ) {
		ERROR("SDL_CreateWindow failed to initialize")
		ERROR(SDL_GetError())
		SDL_Quit(); // Clean up SDL, because it is already initialized.
		return GameStatus::ERROR;
	}
	DEBUG("SDL_CreateWindow successful");

	// Create SDL Renderer and setup parameters
	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if ( m_renderer == nullptr ) {
		ERROR("SDL_CreateRenderer failed to initialize");
		ERROR(SDL_GetError());
		SDL_DestroyWindow(m_window);
		SDL_Quit();
		return GameStatus::ERROR;
	}
	DEBUG("SDL_CreateRenderer successful");

	SDL_RenderSetLogicalSize(m_renderer, m_width, m_height);
	SDL_SetRenderDrawColor(m_renderer, 255, 0, 0, 255);
	m_resources.setRenderer(m_renderer);

	int imgFlags = IMG_INIT_PNG;
	if ( !(IMG_Init(imgFlags) & imgFlags) ) {
		ERROR("IMG_Init failed to initialize");
		ERROR(IMG_GetError());
		SDL_DestroyWindow(m_window);
		SDL_DestroyRenderer(m_renderer);
		SDL_Quit();
		return GameStatus::ERROR;
	}
	DEBUG("IMG_Init successful");

	if ( TTF_Init() == -1 ) {
		ERROR("TTF_Init failed to initialize");
		ERROR(TTF_GetError());
		SDL_DestroyWindow(m_window);
		SDL_DestroyRenderer(m_renderer);
		SDL_Quit();
		return GameStatus::ERROR;
	}
	DEBUG("TTF_Init successful");

	DEBUG("Game::init succesful");
	return GameStatus::RUNNING;
}

GameStatus Game::run() {
	m_status = init();
	if ( m_status == GameStatus::ERROR ) {
		return GameStatus::ERROR;
	}

	const int UPDATES_PER_SECOND = 60;
	const int SKIP_UPDATES = 1000 / UPDATES_PER_SECOND;
	const int MAX_FRAMESKIP = 10;
	const int FRAME_LOCK = 120;

	SDL_Event Event;
	unsigned int NextUpdateTick = SDL_GetTicks();
	int Loops;

	this->changeState(Intro::Instance());

	int fps = 0, ups = 0;
	unsigned int start = SDL_GetTicks();

	DEBUG("Start of loop");

	while ( m_status != GameStatus::STOPPING ) {
		// TODO fixed time step loop
		Loops = 0;

		while ( SDL_GetTicks() > NextUpdateTick && Loops < MAX_FRAMESKIP ) {
			handleEvents();
			update();
			ups++;

			NextUpdateTick += SKIP_UPDATES;
			Loops++;
		}

		render();
		fps++;

		if ( SDL_GetTicks() - start >= 1000 ) {
			float fFps = (fps / (float) (SDL_GetTicks() - start)) * 1000;
			float fUps = (ups / (float) (SDL_GetTicks() - start)) * 1000;
			std::cout << "UPS: " << fUps << " / FPS: " << fFps << std::endl;
			fps = 0, ups = 0;
			start = SDL_GetTicks();
		}	
	}

	for (auto &s : m_states) {
		popState();
	}

	INFO("Exiting application");

	SDL_DestroyWindow(m_window);
	SDL_DestroyRenderer(m_renderer);
	SDL_Quit();
	return GameStatus::STOPPING;
}

void Game::handleEvents() {
	m_states.back()->handleInput(this);
}

void Game::update() {
	// Poll for all events
	m_states.back()->update(this);
}

void Game::render() {
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
	SDL_RenderClear(m_renderer);

	m_states.back()->render(this);

	SDL_RenderPresent(m_renderer);
}

void Game::changeState( State* state ) {
	if ( !m_states.empty() ) {
		m_states.back()->exit(this);
		m_states.pop_back();
	}

	m_states.push_back(state);
	m_states.back()->enter(this);
}

void Game::pushState( State* state ) {
	m_states.push_back(state);
	m_states.back()->enter(this);
}

void Game::popState() {
	if ( !m_states.empty() ) {
		m_states.back()->exit(this);
		m_states.pop_back();
	}
}
