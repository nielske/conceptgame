/*
 * Intro.cpp
 *
 *  Created on: Mar 16, 2018
 *      Author: niels
 */

#include "State_Intro.h"

#include <SDL2/SDL_events.h>

#include "Game.h"
#include "ResourceManager.h"

Intro Intro::m_Intro; // @suppress("Abstract class cannot be instantiated")
const Uint8* keyState;

void Intro::enter( Game* game ) {
	m_world = game->getWorld();
	m_world->setRenderer(game->getRenderer());
	m_world->setRM(game->getRM());
	m_world->setSettings(game->getSettings());

	game->getRM()->addTexture("penguin.png");
	game->getRM()->addTexture("belt.png");
	game->getRM()->addTexture("grass.png");
	game->getRM()->addTexture("water.png");
	game->getRM()->addTexture("sand.png");
	game->getRM()->addTexture("placeholder_inbound_mag.png");
	game->getRM()->addFont("UbuntuMono-R.ttf");

	m_world->generate();
}

void Intro::exit( Game* game ) {
	DEBUG("EXIT STATE INTRO")
	m_world->cleanup();
}

void Intro::handleInput( Game* game ) {
	SDL_Event event;

	while ( SDL_PollEvent(&event) ) {
		switch ( event.type ) {
			case SDL_QUIT:
				game->setStatus(GameStatus::STOPPING);
				break;
		}
	}

	keyState = SDL_GetKeyboardState(NULL);
	if ( keyState[SDL_SCANCODE_ESCAPE] ) {
		game->setStatus(GameStatus::STOPPING);
	} else if ( keyState[SDL_SCANCODE_W] ) {
		m_world->setPosition(COORDINATE(m_world->getPosition().first, m_world->getPosition().second - 1));
	} else if ( keyState[SDL_SCANCODE_A] ) {
		m_world->setPosition(COORDINATE(m_world->getPosition().first - 1, m_world->getPosition().second));
	} else if ( keyState[SDL_SCANCODE_S] ) {
		m_world->setPosition(COORDINATE(m_world->getPosition().first, m_world->getPosition().second + 1));
	} else if ( keyState[SDL_SCANCODE_D] ) {
		m_world->setPosition(COORDINATE(m_world->getPosition().first + 1, m_world->getPosition().second));
	}
}

void Intro::update( Game* game ) {
	m_world->update();
}

void Intro::render( Game* game ) {
	// for ( int x = 0; x < (game->getWindowSize().first / m_world->m_tileSize); x++ ) {
	// 	for ( int y = 0; y < (game->getWindowSize().second / m_world->m_tileSize); y++ ) {
	// 		Tile* tile = m_world->getTile(x, y);
	// 		tile->render(x * m_world->m_tileSize, y * m_world->m_tileSize);
	// 	}
	// }

	m_world->render();

	if ( keyState[SDL_SCANCODE_M] ) {
		SDL_Rect clip = {0, 0, 500, 500};
		game->getRM()->getTexture("minimap_blank")->render(250, 250, &clip);
		game->getRM()->getTexture("minimap")->render(250, 250, &clip);
	}
}
