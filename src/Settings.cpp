/*
 * Settings.cpp
 *
 *  Created on: Jun 1, 2018
 *      Author: niels
 */

#include <iostream>
#include <fstream>
#include <SDL2/SDL.h>
#include <chrono>

#include "Settings.h"
#include "Utility.h"

Settings::Settings() :
		m_isChanged(false) {
}

Settings::~Settings() {
	save();
}

bool Settings::load( const std::string& file ) {
	m_data.clear();
	m_file = file;
	return read();
}

bool Settings::save() {
	if ( m_isChanged ) {
		m_isChanged = false;
		return write();
	}

	return true;
}

bool Settings::read() {
	std::string config = Utility::DATA_PATH + "/config.ini";
	bool exists = false;

	std::ifstream in(m_file);
	if ( !in.is_open() ) {
		if ( FILE *file = fopen(config.c_str(), "r") ) {
			fclose(file);
			exists = true;
		} else {
			// Generate default config
			std::ofstream out(config);

			std::chrono::system_clock::time_point now;

			out << "# Configuration file automatically generated on ";
			out << Utility::getDateTime() << std::endl;
			out << std::endl;

			// Get display information
			SDL_DisplayMode dm;
			for ( int i = 0; i < SDL_GetNumVideoDisplays(); i++ ) {
				int shouldBeZero = SDL_GetCurrentDisplayMode(i, &dm);

				if ( shouldBeZero != 0 ) {
					// Error
					ERROR("Could not get display mode.")
					ERROR(SDL_GetError())
				} else {
					out << "r_width: " << dm.w << std::endl;
					out << "r_height: " << dm.h << std::endl;
					break;
				}
			}

			out << "r_fullscreen: true" << std::endl;

			out.close();
			in.open(m_file);

			if ( !in.is_open() ) {
				exists = true;
			} else {
				exists = false;
			}
		}
	} else {
		exists = true;
	}

	if ( !exists ) {
		return false;
	}

	std::string line;
	while ( std::getline(in, line) ) {
		std::pair<std::string, std::string> keyValuePair = parseLine(line);

		if ( !keyValuePair.first.empty() ) {
			m_data[keyValuePair.first] = keyValuePair.second;
		}
	}

	in.close();
	m_isChanged = false;

	return true;
}

bool Settings::write() const {
	std::vector<std::pair<std::string, std::string>> fileContents;
	std::ifstream in(m_file);

	if ( in.is_open() ) {
		std::string line;

		while ( std::getline(in, line) ) {
			std::pair<std::string, std::string> keyValuePair = parseLine(line);

			if ( !keyValuePair.first.empty() ) {
				auto it = m_data.find(keyValuePair.first);

				if ( it != m_data.end() ) {
					keyValuePair.second = it->second;
				}
			} else {
				keyValuePair.first = line;
			}
			fileContents.push_back(keyValuePair);
		}
	} else {
		for ( auto it = m_data.begin(); it != m_data.end(); ++it ) {
			fileContents.push_back(std::make_pair(it->first, it->second));
		}
	}

	in.close();

	std::ofstream out(m_file);

	if ( !out.is_open() ) {
		ERROR("Unable to open configuration file for writing.");
		return false;
	}

	for ( auto it = fileContents.begin(); it != fileContents.end(); ++it ) {
		out << it->first;

		if ( !it->second.empty() ) {
			out << ": " << it->second;
		}

		out << std::endl;
	}

	out.close();
	return true;
}

std::pair<std::string, std::string> Settings::parseLine( const std::string& line ) const {
	if ( line.size() > 0 && line[0] != '#' ) {
		size_t index = 0;

		// Trim leading whitespace
		while ( std::isspace(line[index], m_locale) ) {
			index++;
		}

		// Get key string
		const size_t beginKeyString = index;

		while ( !std::isspace(line[index], m_locale) && line[index] != ':' ) {
			index++;
		}

		const std::string key = line.substr(beginKeyString, index - beginKeyString);

		// Skip assignment
		while(std::isspace(line[index], m_locale) || line[index] == ':') {
			index++;
		}

		// Get value string
		const std::string value = line.substr(index, line.size() - index);

		// Return key value pair
		return std::make_pair(key, value);
	}

	// If line empty or comment, return empty pair
	return std::make_pair(std::string(), std::string());
}

void Settings::print() const {
	for (auto &element : m_data) {
		std::string str = element.first + ": " + element.second;
		DEBUG(str)
	}

	std::string str = "Size: " + m_data.size();
	DEBUG(str)
}

bool Settings::isChanged() const {
	return m_isChanged;
}
