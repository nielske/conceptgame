/*
 * Texture.cpp
 *
 *  Created on: Mar 17, 2018
 *      Author: niels
 */

#include "Texture.h"
#include "Utility.h"
#include <SDL2_image/SDL_image.h>
#include <iostream>

Texture::Texture( SDL_Renderer* renderer, int width, int height, SDL_Texture* texture ) :
		m_renderer(renderer) {
//	if (width && height && texture != nullptr) {
		m_width = width;
		m_height = height;
		m_texture = texture;
//	}
}

Texture::~Texture() {
	freeMemory();
}

int Texture::getWidth() {
	return m_width;
}

int Texture::getHeight() {
	return m_height;
}

bool Texture::loadFromFile( std::string path ) {
	freeMemory();

	SDL_Texture* newTexture = nullptr;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());

	if ( !loadedSurface || loadedSurface == nullptr ) {
		ERROR("Error loading image: " + path);
		ERROR(IMG_GetError());
		return false;
	} else {
		newTexture = SDL_CreateTextureFromSurface(m_renderer, loadedSurface);
		if ( newTexture == nullptr ) {
			ERROR("Error loading image: " + path);
			return false;
		} else {
			m_width = loadedSurface->w;
			m_height = loadedSurface->h;
		}
		SDL_FreeSurface(loadedSurface);
	}

	m_texture = newTexture;
	return m_texture != nullptr;
}

bool Texture::loadFromRenderedText( std::string renderedText, SDL_Color textColor ) {
	freeMemory();
	return false;
}

void Texture::freeMemory() {
	if ( m_texture != nullptr ) {
		SDL_DestroyTexture(m_texture);
		m_texture = nullptr;
		m_width = 0;
		m_height = 0;
	}
}

void Texture::setAlpha( Uint8 alpha ) {
	SDL_SetTextureAlphaMod(m_texture, alpha);
}

void Texture::setRenderer( SDL_Renderer* renderer ) {
	m_renderer = renderer;
}

void Texture::render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip ) {
	SDL_Rect renderQuad = { x, y, 64, 64 };

	if ( clip != nullptr ) {
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	if ( m_texture ) {
		SDL_RenderCopyEx(m_renderer, m_texture, clip, &renderQuad, angle, center, flip);
	} else {
		SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
		SDL_RenderDrawRect(m_renderer, &renderQuad);
		return;
	}
}
