/*
 * ResourceManager.cpp
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#include "ResourceManager.h"
#include "Utility.h"
#include <assert.h>
#include <string>

ResourceManager::ResourceManager() :
		m_renderer(nullptr) {
}

ResourceManager::~ResourceManager() {
	cleanup();
}

void ResourceManager::cleanup() {
	for ( auto tex : m_textures ) {
		delete tex.second;
		tex.second = nullptr;
	}

	m_renderer = nullptr;
}

bool ResourceManager::addTexture( const std::string texture ) {
	std::string path = Utility::DATA_PATH + "textures/" + texture;
	Texture* tex = new Texture;
	tex->setRenderer(m_renderer);

	if ( tex->loadFromFile(path) ) {
		auto inserted = m_textures.insert(std::make_pair(texture, tex));
		if ( !inserted.second ) {
			ERROR("Failed to add texture: " + texture + " to the map.")
			return false;
		}
	} else {
		ERROR("Failed to add texture: " + texture + " to the map.")
		return false;
	}

	DEBUG("Added texture: " + texture + " to the map.")
	return true;
}

bool ResourceManager::addTexture( const std::string resource, Texture* texture ) {
	auto inserted = m_textures.insert(std::make_pair(resource, texture));
	if ( !inserted.second ) {
		ERROR("Failed to add texture: " + resource + " to the map.")
		return false;
	}

	DEBUG("Added texture: " + resource + " to the map.")
    return true;
}

bool ResourceManager::addFont( const std::string resource ) {
	std::string path = Utility::DATA_PATH + "/" + resource;

	TTF_Font* font;
	font = TTF_OpenFont(path.c_str(), 16);

	if ( !font ) {
		ERROR("Failed to add font: " + resource + " to the map.")
		ERROR(TTF_GetError());
		return false;
	}

	auto inserted = m_font.insert(std::make_pair(resource, font));
	if ( !inserted.second ) {
		ERROR("Failed to add font: " + resource + " to the map.")
		return false;
	}

	DEBUG("Added font: " + resource + " to the map.")
	return true;
}

bool ResourceManager::removeTexture( const std::string texture ) {
	auto find = m_textures.find(texture);

	if( find == m_textures.end() ) {
		ERROR("Cannot find texture: " + texture + " in map.")
		return false;
	}

	m_textures.at(texture)->freeMemory();
	delete m_textures.at(texture);
	m_textures.erase(texture);
}

Texture* ResourceManager::getTexture( const std::string texture ) {
	auto find = m_textures.find(texture);

	if ( find == m_textures.end() ) {
		ERROR("Cannot find texture: " + texture + " in map.")
		return nullptr;
	}

	return find->second;
}

TTF_Font* ResourceManager::getFont( const std::string resource ) {
	auto find = m_font.find(resource);

	if ( find == m_font.end() ) {
		if ( m_font.size() != 0 ) {
			ERROR("Cannot find font: " + resource + " in map. Using default.")
			return m_font.at(0);
		} else {
			ERROR("Cannot find any fonts in map.")
			return nullptr;
		}
	}

	return find->second;
}
