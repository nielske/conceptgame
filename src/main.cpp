/*
 * main.cpp
 *
 *  Created on: Mar 3, 2018
 *      Author: niels
 */

#include "Game.h"
#include <fstream>

std::string Utility::BIN_PATH;
std::string Utility::ROOT_PATH;
std::string Utility::EXE_PATH;
std::string Utility::DATA_PATH;
const std::string Utility::APPLICATION_VERSION = "1.0";

// Check operating system
// TODO better os detection
#ifdef __APPLE__
const std::string Utility::OPERATING_SYSTEM = "Mac OS";
#elif __unix__
const std::string Utility::OPERATING_SYSTEM = Utility::getLinuxOperatingSystem();
#elif _WIN64
const std::string Utility::OPERATING_SYSTEM = "Windows 64-bit";
#elif _WIN32
const std::string Utility::OPERATING_SYSTEM = "Windows 32-bit";
#else
const std::string Utility::OPERATING_SYSTEM = "Undefined operating system";
#endif

int main( int argc, char * argv[] ) {
    // Get the necessary paths
    std::string path = SDL_GetBasePath();

#ifdef __APPLE__
	// [ROOT]/bin/
	Utility::BIN_PATH = path;
	// [ROOT]/bin/ConceptGame
	Utility::EXE_PATH = path + "ConceptGame";
	// [ROOT]/
	Utility::ROOT_PATH = path;
	for (int i = 0; i != 4; i++)
		Utility::ROOT_PATH.pop_back();
	// [ROOT]/data/
	Utility::DATA_PATH = Utility::ROOT_PATH + "data/";
#elif _WIN32
	// TODO check if working on different windows systems
	//	exePath = path;
	//	binPath = path.substr(0, path.find_last_of('\\'));
	//	rootPath = binPath.substr(0, binPath.find_last_of('\\'));
	return 0;
#elif __unix__
    Utility::BIN_PATH = path.substr(0, path.find_last_of('/'));
    Utility::ROOT_PATH = Utility::BIN_PATH.substr(0, Utility::BIN_PATH.find_last_of('/'));
    Utility::EXE_PATH = path;
    Utility::DATA_PATH = Utility::ROOT_PATH + "/data";
    // TODO check if working on different linux systems
#endif

    Game Game;
    GameStatus status = Game.run();

    if ( status == GameStatus::ERROR ) {
        return 0; // Bad exit
    }

    return 1; // Good exit
}
