/*
 * RenderComponent.cpp
 *
 *  Created on: Jun 22, 2018
 *      Author: niels
 */

#include "RenderComponent.h"

RenderComponent::RenderComponent(Texture* texture) {
	m_texture = texture;
}

RenderComponent::~RenderComponent() {
	m_texture = nullptr;
}

void RenderComponent::update() {

}

void RenderComponent::render(int x, int y) {
	m_texture->render(x, y);
}
