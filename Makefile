CXX ?= g++

# PATHS
SRC_PATH = src
BUILD_PATH = build
BIN_PATH = $(BUILD_PATH)/bin

# EXECUTABLE
EXE_NAME = ConceptGame

# EXTENSIONS
SRC_EXT = cpp

# CODE LISTS
SOURCES = $(wildcard $(SRC_PATH)/*.$(SRC_EXT))
#SOURCES = $(shell find $(SRC_PATH) -name '*.$(SRC_EXT)' | sort -k 1nr | cut -f2-)
#OBJECTS = $(patsubst %.cpp, %.o, $(SOURCES))
OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(SRC_EXT)=$(BUILD_PATH)/%.o)
DEPS = $(OBJECTS:.o=.d)

# FLAGS
COMPILE_FLAGS = -std=c++11 -g
INCLUDES = -Iinclude/
LIBS = -framework SDL2 -lnoise -framework SDL2_image -framework SDL2_ttf

all: $(BIN_PATH)/$(EXE_NAME)
$(BIN_PATH)/$(EXE_NAME): $(OBJECTS)
	mkdir -p $(BIN_PATH)
	$(CXX) $(LIBS) -o $@ $^ -F/Library/Frameworks

$(BUILD_PATH)/%.o: $(SRC_PATH)/%.cpp
	$(CXX) $(CXXFLAGS) $(COMPILE_FLAGS) $(INCLUDES) -c $< -o $@

.PHONY: clean all
clean:
	@echo "Deleting $(EXE_NAME)..."
	$(RM) $(EXE_NAME)
	@echo "Deleting build directories..."
	$(RM) -r $(BUILD_PATH)/*.d
	$(RM) -r $(BUILD_PATH)/*.o
	$(RM) -r $(BIN_PATH)
